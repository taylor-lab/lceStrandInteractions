import os
import sys
from pathlib import Path
import pandas as pd

# based on Snakefile.kucab.smk

envvars:
  "projectRoot"

configfile:
    os.environ['projectRoot']+"config/config.yaml"

sasFile = os.environ['projectRoot']+"data/mu/lceSAS.tab"

sas = pd.read_csv(sasFile)
sasSelect = sas[~sas['cleanDiagnosis'].isin(['normal']) & sas['cause'].isin(['DEN']) & sas['strain'].isin(['c3h'])]

NODS = list(sasSelect['nodId'])
NODSALL = list(sas['nodId'])
NODSTRAIN = dict(zip(sasSelect['nodId'],sasSelect['strain']))

rule multiRegress:
  input:
    tricomp="{genome}.minusBlacklist.windows.w{win}.s{step}.triComp"
  output: 
    refout="{genome}.{rfd}.w{win}.s{step}.multiRegress.pdf"
  resources:
    mem_mb=64000,
    resources='"rusage[mem=64000]"',
    time="6:00"
  params:
    rscript=os.environ['projectRoot']+config['src']+"multiRegressionAnalysis.R"
  threads: 1
  conda:
    os.environ['projectRoot']+config['envsDir']+"base.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"mr.{genome}.{rfd}.{win}.{step}.o",
    e=os.environ['projectRoot']+config['logsDir']+"mr.{genome}.{rfd}.{win}.{step}.e"
  shell:
    """
    Rscript --vanilla {params.rscript} {output.refout} {input.tricomp} > {log.o} 2> {log.e}
    """




rule tricompPerGenomeWindow:
  input:
    bed="{genome}.minusBlacklist.windows.w{win}.s{step}.bed",
    fa=os.environ['projectRoot']+config['genomeDir']+"{genome}.fa"
  output:
    refout="{genome}.minusBlacklist.windows.w{win}.s{step}.triComp"
  resources:
    mem_mb=8000,
    resources='"rusage[mem=8000]"',
    time="1:00"
  params:
    pl=os.environ['projectRoot']+config['src']+"streamTriNucCounterNoMerge.pl"
  threads: 1
  conda:
    os.environ['projectRoot']+config['envsDir']+"samtools.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"tpwg.{genome}.{win}.{step}.o",
    e=os.environ['projectRoot']+config['logsDir']+"tpwg.{genome}.{win}.{step}.e"
  shell:
    """
    bedtools getfasta -tab -name+ -fi {input.fa} -bed {input.bed} | perl {params.pl} > {output.refout} 2>> {log.e} 
    """

rule subtractBlacklist:
  input:
    bed="{genome}.windows.w{win}.s{step}.bed",
    blacklist=os.environ['projectRoot']+config['genomeDir']+"{genome}.arc.blacklist.bed"
  output:
    refout="{genome}.minusBlacklist.windows.w{win}.s{step}.bed"
  resources:
    mem_mb=2000,
    resources='"rusage[mem=2000]"',
    time="1:00"
  threads: 1
  conda:
    os.environ['projectRoot']+config['envsDir']+"samtools.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"sbl.{genome}.{win}.{step}.o",
    e=os.environ['projectRoot']+config['logsDir']+"sbl.{genome}.{win}.{step}.e"
  shell:
    """
    bedtools subtract -a {input.bed} -b {input.blacklist} > {output.refout} 2> {log.e} ;
    """



rule windowsOverGenome:
  input:
    tracking="{genome}.csm.complete",
    chromSize=os.environ['projectRoot']+config['genomeDir']+"{genome}.chromSize.main"
  output:
    refout="{genome}.windows.w{win}.s{step}.bed"
  resources:
    mem_mb=2000,
    resources='"rusage[mem=2000]"',
    time="1:00"
  params:
    window="{win}",
    step="{step}"
  threads: 1
  conda:
    os.environ['projectRoot']+config['envsDir']+"samtools.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"wog.{genome}.{win}.{step}.o",
    e=os.environ['projectRoot']+config['logsDir']+"wog.{genome}.{win}.{step}.e"
  shell:
    """
    bedtools makewindows -w {params.window} -s {params.step} -g {input.chromSize} > {output.refout} 2> {log.e} ;
    """

rule makeChromSizeMain:
  input:
    fa=os.environ['projectRoot']+config['genomeDir']+"{genome}.fa",
  output:
    refout="{genome}.csm.complete",
    csm=os.environ['projectRoot']+config['genomeDir']+"{genome}.chromSize.main"
  resources:
    mem_mb=2000,
    resources='"rusage[mem=2000]"',
    time="1:00"
  conda:
    os.environ['projectRoot']+config['envsDir']+"samtools.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"mkChromSize.{genome}.o",
    e=os.environ['projectRoot']+config['logsDir']+"mkChromSize.{genome}.e"
  shell:
    """
    set +o pipefail;
    samtools faidx {input} 2> {log.e}
    cat {input.fa}.fai | perl -walne '$F[0]=~s/^chr//;if($F[0]=~/^\d+$/){{print"$F[0]\t$F[1]"}}if($F[0]=~/^[XY]$/){{print"$F[0]\t$F[1]"}}' | sort -k1V > {output.csm} 2>> {log.e}
    date > {output.refout}
    """


