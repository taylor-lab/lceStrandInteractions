import os
import sys
import re
import pandas as pd
from pathlib import Path

envvars:
  "projectRoot"

configfile:
  os.environ['projectRoot']+"config/config.yaml"

wildcard_constraints:
  win="\d+",
  step="\d+"


##
# prototyping tools
class proto(object):
  pass

wildcardsTest = proto()
##

edFile = "externalData.csv"
samples = pd.read_csv(edFile).set_index("sample",drop=False)
okSamples = samples[samples['type'].isin(['okseq'])]
repliSamples = samples[samples['type'].isin(['elRepli'])]
LIBTRAIN=["IMR-90.okseq.hg19","K562.okseq.hg19","ES-E14TG2a.okseq.bl6"]
REPTRAIN=["IMR-90.elRepli.hg19","K562.elRepli.hg19","ES-E14TG2a.elRepli.bl6"]

###
# function definitions

def getSamplesOkCov(wildcards):
  subs=okSamples[okSamples['genome'].isin([wildcards.genome])]
  x=list(set(subs["sample"]))
  tp=list([])
  for y in x:
    tp.extend([y+".okseq."+wildcards.genome+".w"+wildcards.win+".s"+wildcards.step+".okAG.cov"])
  return tp 

def getSamplesRepliCov(wildcards):
  subs=repliSamples[repliSamples['genome'].isin([wildcards.genome])]
  x=list(set(subs["sample"]))
  tp=list([])
  for y in x:
    tp.extend([y+".elRepli."+wildcards.genome+".w"+wildcards.win+".s"+wildcards.step+".elAG.cov"])
  return tp 

def getReplicatesOk(wildcards):
  subs=okSamples[okSamples['genome'].isin([wildcards.genome])]
  m=list(subs.loc[[wildcards.type],'fastq'])
  rtList=list([])
  for x in m:
    rtList.extend([x+"."+wildcards.genome+".w"+wildcards.win+".s"+wildcards.step+".cov"])
  return rtList

def getReplicatesRepli(wildcards):
  subs=repliSamples[repliSamples['genome'].isin([wildcards.genome])]
  m=list(subs.loc[[wildcards.type],'fastq'])
  rtList=list([])
  p=re.compile(".*b[0-9]+.t[0-9]+")
  for x in m:
    n=p.search(x)
    z=n.group(0)
    rtList.extend([z+"."+wildcards.genome+".w"+wildcards.win+".s"+wildcards.step+".cov"])
  rt=list(set(rtList))
  return rt

###

rule all:
  input:
    "doOk.hg19.w1000.s1000.complete",
    "doRepli.hg19.w50000.s50000.complete",
    "doOk.bl6.w1000.s1000.complete",
    "doRepli.bl6.w50000.s50000.complete",
    "IMR_K562_E14TG2a.n17l85we2r0h1.w50000.s10000.Rmodel",
    "RPE-hTERT.hg19.IMR_K562_E14TG2a.n17l85w0e2r0h1.w50000.s10000.validate.pdf",
    "RPE-hTERT.hg19.IMR_K562_E14TG2a.n17l80w0e2r0h1.w50000.s10000.validate.pdf",
    "RPE-hTERT.hg19.IMR_K562_E14TG2a.n17l75w0e2r0h1.w50000.s10000.validate.pdf",
    "RPE-hTERT.hg19.IMR_K562_E14TG2a.n17l85w0e2r0h0.w50000.s10000.validate.pdf",
    "RPE-hTERT.hg19.IMR_K562_E14TG2a.n17l0w5e2r0h1.w50000.s10000.validate.pdf",
    "RPE-hTERT.hg19.IMR_K562_E14TG2a.n17l0w3e2r0h1.w50000.s10000.validate.pdf",
    "RPE-hTERT.hg19.IMR_K562_E14TG2a.n15l85w0e2r0h1.w50000.s10000.validate.pdf",
    "RPE-hTERT.hg19.IMR_K562_E14TG2a.n13l85w0e2r0h1.w50000.s10000.validate.pdf",
    "RPE-hTERT.hg19.IMR_K562_E14TG2a.n17l85w0e1r0h1.w50000.s10000.validate.pdf"
  output:
    refout="all.out"
  shell:
    """
    date > {output.refout}
    """

rule elStratCoverage:
  input:
    "{prefix}.elAG.cov"
  output:
    refout="{prefix}.elAG.elStrat{nstrat}.bed",
    medians="{prefix}.elAG.elStrat{nstrat}.bed.stateMedians"
  resources:
    mem_mb=8000,
    resources='"rusage[mem=8000]"',
    time="2:00"
  conda:
    os.environ['projectRoot']+config['envsDir']+"base.yaml"
  params:
    rscript=os.environ['projectRoot']+config['src']+"elAGcovToBed.R"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"elCov{prefix}.s{nstrat}.o",
    e=os.environ['projectRoot']+config['logsDir']+"elCov{prefix}.s{nstrat}.e"
  shell:
    """
    Rscript --vanilla {params.rscript} {output.refout} {input} > {log.o} 2> {log.e}
    """


rule generateFigEdRepliseq:
  input:
    "ES-E14TG2a.c3h.w50000.s10000.elRepToRfd.normES-E14TG2a.Rtab",
    "1-6.c3h.w50000.s10000.elRepToRfd.normTUBBS.Rtab",
    "RPE-hTERT.hg19.w50000.s10000.elRepToRfd.normRPE-hTERT.Rtab",
    "7889SA3.hg19.w50000.s10000.elRepToRfd.normRPE-hTERT.Rtab",
    "GM12878.hg19.w50000.s10000.elRepToRfd.normRPE-hTERT.Rtab",
    "H1-hESC.hg19.w50000.s10000.elRepToRfd.normRPE-hTERT.Rtab",
    "H9.hg19.w50000.s10000.elRepToRfd.normRPE-hTERT.Rtab",
    "HCT116.hg19.w50000.s10000.elRepToRfd.normRPE-hTERT.Rtab",
    "HEK293.hg19.w50000.s10000.elRepToRfd.normRPE-hTERT.Rtab",
    "IMR-90.hg19.w50000.s10000.elRepToRfd.normRPE-hTERT.Rtab",
    "K562.hg19.w50000.s10000.elRepToRfd.normRPE-hTERT.Rtab",
    "RPE-hTERT.hg19.w50000.s10000.elRepToRfd.normRPE-hTERT.Rtab",
    "WTC-11.hg19.w50000.s10000.elRepToRfd.normRPE-hTERT.Rtab"
  output:
    refout="figEd_repliSeqToOkRel{ver}.pdf"
  resources:
    mem_mb=8000,
    resources='"rusage[mem=8000]"',
    time="2:00"
  conda:
    os.environ['projectRoot']+config['envsDir']+"base.yaml"
  params:
    # input files are hard-coded in figEdRepliseq.R as there are too many to be
    # practical for dynamic coding and commandline passing
    rscript=os.environ['projectRoot']+config['src']+"figEdRepliseq.R"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"figEdRepliseq{ver}.o",
    e=os.environ['projectRoot']+config['logsDir']+"figEdRepliseq{ver}.e"
  shell:
    """
    Rscript --vanilla {params.rscript} {output.refout} > {log.o} 2> {log.e}
    """


rule generatePSCconsensusRfd:
  input:
    "7889SA3.hg19.w50000.s10000.elRepToRfd.normRPE-hTERT.Rtab",
    "H9.hg19.w50000.s10000.elRepToRfd.normRPE-hTERT.Rtab",
    "H1-hESC.hg19.w50000.s10000.elRepToRfd.normRPE-hTERT.Rtab",
    "WTC-11.hg19.w50000.s10000.elRepToRfd.normRPE-hTERT.Rtab"
  output:
    refout="hg19.PSCconsensus.rfdStrat{nstrat}.bed"
  resources:
    mem_mb=8000,
    resources='"rusage[mem=8000]"',
    time="2:00"
  conda:
    os.environ['projectRoot']+config['envsDir']+"base.yaml"
  params:
    rscript=os.environ['projectRoot']+config['src']+"rfdConsensusToBed.R"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"pscConsRfd.{nstrat}.o",
    e=os.environ['projectRoot']+config['logsDir']+"pscConsRfd.{nstrat}.e"
  shell:
    """
    Rscript --vanilla {params.rscript} {output.refout} {input} > {log.o} 2> {log.e}
    """

rule generateMouseLiverConsensus:
  input:
    "Hep74-3a.c3h.w50000.s10000.elRepToRfd.normTUBBS.Rtab",
    "1-6.c3h.w50000.s10000.elRepToRfd.normTUBBS.Rtab"
  output:
    refout="c3h.mouseLiver.rfdStrat{nstrat}.bed",
    okFilt="c3h.mouseLiverOkFilt.rfdStrat{nstrat}.bed",
  resources:
    mem_mb=8000,
    resources='"rusage[mem=8000]"',
    time="2:00"
  conda:
    os.environ['projectRoot']+config['envsDir']+"base.yaml"
  params:
    rscript=os.environ['projectRoot']+config['src']+"rfdConsensusToBed.R",
    rfilt=os.environ['projectRoot']+config['src']+"rfdConsensusWithOkToBed.R"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"c3h.pscConsRfd.{nstrat}.o",
    e=os.environ['projectRoot']+config['logsDir']+"c3h.pscConsRfd.{nstrat}.e"
  shell:
    """
    Rscript --vanilla {params.rscript} {output.refout} {input} > {log.o} 2> {log.e}
    Rscript --vanilla {params.rfilt} {output.okFilt} {input} >> {log.o} 2>> {log.e}
    """

rule generateMouseLiverConsensus3:
  input:
    "Hep74-3a.c3h.w50000.s10000.elRepToRfd.normTUBBS.Rtab",
    "Hep74-3a.c3h.w50000.s10000.elRepToRfd.normTUBBS.Rtab"
  output:
    refout="c3h.mouseLiver3A.rfdStrat{nstrat}.bed",
    okFilt="c3h.mouseLiver3AOkFilt.rfdStrat{nstrat}.bed"
  resources:
    mem_mb=8000,
    resources='"rusage[mem=8000]"',
    time="2:00"
  conda:
    os.environ['projectRoot']+config['envsDir']+"base.yaml"
  params:
    rscript=os.environ['projectRoot']+config['src']+"rfdConsensusToBed.R",
    rfilt=os.environ['projectRoot']+config['src']+"rfdConsensusWithOkToBed.R"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"c3h3A.pscConsRfd.{nstrat}.o",
    e=os.environ['projectRoot']+config['logsDir']+"c3h3A.pscConsRfd.{nstrat}.e"
  shell:
    """
    Rscript --vanilla {params.rscript} {output.refout} {input} > {log.o} 2> {log.e}
    Rscript --vanilla {params.rfilt} {output.okFilt} {input} >> {log.o} 2>> {log.e}
    """


rule generateMouseLiverConsensus1:
  input:
    one="1-6.c3h.w50000.s10000.elRepToRfd.normTUBBS.Rtab",
    two="1-6.c3h.w50000.s10000.elRepToRfd.normTUBBS.Rtab",
    #cov="1-6.elRepli.c3h.w50000.s10000.elAG.cov"
  output:
    refout="c3h.mouseLiver1.rfdStrat{nstrat}.bed",
    okFilt="c3h.mouseLiver1OkFilt.rfdStrat{nstrat}.bed",
    #elbed="c3h.mouseLiver1.elStrat{nstrat}.bed"
  resources:
    mem_mb=8000,
    resources='"rusage[mem=8000]"',
    time="2:00"
  conda:
    os.environ['projectRoot']+config['envsDir']+"base.yaml"
  params:
    rscript=os.environ['projectRoot']+config['src']+"rfdConsensusToBed.R",
    rfilt=os.environ['projectRoot']+config['src']+"rfdConsensusWithOkToBed.R"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"c3h1.pscConsRfd.{nstrat}.o",
    e=os.environ['projectRoot']+config['logsDir']+"c3h1.pscConsRfd.{nstrat}.e"
  shell:
    """
    Rscript --vanilla {params.rscript} {output.refout} {input} > {log.o} 2> {log.e}
    Rscript --vanilla {params.rfilt} {output.okFilt} {input} >> {log.o} 2>> {log.e}
    """



rule runContinueRfd:
  input:
    "RPE-hTERT.hg19.w50000.s10000.elRepToRfd.normRPE-hTERT.Rtab",
    "IMR-90.hg19.w50000.s10000.elRepToRfd.normRPE-hTERT.Rtab",
    "HEK293.hg19.w50000.s10000.elRepToRfd.normRPE-hTERT.Rtab",
    "HCT116.hg19.w50000.s10000.elRepToRfd.normRPE-hTERT.Rtab",
    "H9.hg19.w50000.s10000.elRepToRfd.normRPE-hTERT.Rtab",
    "H1-hESC.hg19.w50000.s10000.elRepToRfd.normRPE-hTERT.Rtab",
    "GM12878.hg19.w50000.s10000.elRepToRfd.normRPE-hTERT.Rtab",
    "7889SA3.hg19.w50000.s10000.elRepToRfd.normRPE-hTERT.Rtab",
    "WTC-11.hg19.w50000.s10000.elRepToRfd.normRPE-hTERT.Rtab",
    "K562.hg19.w50000.s10000.elRepToRfd.normRPE-hTERT.Rtab",
    "RPE-hTERT.hg19.w50000.s10000.elRepToRfd.normGM06990.Rtab",
    "IMR-90.hg19.w50000.s10000.elRepToRfd.normGM06990.Rtab",
    "HEK293.hg19.w50000.s10000.elRepToRfd.normGM06990.Rtab",
    "HCT116.hg19.w50000.s10000.elRepToRfd.normGM06990.Rtab",
    "H9.hg19.w50000.s10000.elRepToRfd.normGM06990.Rtab",
    "H1-hESC.hg19.w50000.s10000.elRepToRfd.normGM06990.Rtab",
    "GM12878.hg19.w50000.s10000.elRepToRfd.normGM06990.Rtab",
    "7889SA3.hg19.w50000.s10000.elRepToRfd.normGM06990.Rtab",
    "WTC-11.hg19.w50000.s10000.elRepToRfd.normGM06990.Rtab",
    "K562.hg19.w50000.s10000.elRepToRfd.normGM06990.Rtab",
    "1-6.c3h.w50000.s10000.elRepToRfd.normTUBBS.Rtab",
    "3A.c3h.w50000.s10000.elRepToRfd.normTUBBS.Rtab",
    "ES-E14TG2a.c3h.w50000.s10000.elRepToRfd.normTUBBS.Rtab",
    "1-6.c3h.w50000.s10000.elRepToRfd.normES-E14TG2a.Rtab",
    "3A.c3h.w50000.s10000.elRepToRfd.normES-E14TG2a.Rtab",
    "ES-E14TG2a.c3h.w50000.s10000.elRepToRfd.normES-E14TG2a.Rtab",
    "Hep74-3a.c3h.w50000.s10000.elRepToRfd.normTUBBS.Rtab"
  output:
    refout="runContinueRfd.out"
  resources:
    mem_mb=2000,
    resources='"rusage[mem=2000]"',
    time="2:00"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"contRfdComplete.o",
    e=os.environ['projectRoot']+config['logsDir']+"contRfdComplete.e"
  shell:
    """
    date > {output.refout}
    """

rule continueRfd:
  input:
    okseq="{ok}.okseq.{genome}.w1000.s1000.okAG.cov",
    el="{el}.elRepli.{genome}.w{win}.s{step}.elAG.cov"
  output:
    refout="{el}.{genome}.w{win}.s{step}.elRepToRfd.norm{ok}.Rtab"
  resources:
    mem_mb=18000,
    resources='"rusage[mem=18000]"',
    time="2:00"
  conda:
    os.environ['projectRoot']+config['envsDir']+"base.yaml"
  params:
    rscript=os.environ['projectRoot']+config['src']+"repliseqToRfdNorm.R"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"elRepToRfd.rep{el}.ok{ok}.{genome}.w{win}.s{step}.o",
    e=os.environ['projectRoot']+config['logsDir']+"elRepToRfd.rep{el}.ok{ok}.{genome}.w{win}.s{step}.e"
  shell:
    """
    Rscript --vanilla {params.rscript} {output.refout} {input.okseq} {input.el} > {log.o} 2> {log.e}
    """


rule validateTrainedModel:
  # use trained model to predict some held-out sample.
  # Note - validation libriary pairs are currently hard-coded.
  input:
    model="{trainset}.{model}.w{win}.s{step}.Rmodel",
    okseq="{val}.okseq.{genome}.w1000.s1000.okAG.cov",
    el="{val}.elRepli.{genome}.w{win}.s{step}.elAG.cov"
  output:
    refout="{val}.{genome}.{trainset}.{model}.w{win}.s{step}.validate.bed",
    figures="{val}.{genome}.{trainset}.{model}.w{win}.s{step}.validate.pdf"
  resources:
    mem_mb=16000,
    resources='"rusage[mem=16000]"',
    time="2:00"
  conda:
    os.environ['projectRoot']+config['envsDir']+"base.yaml"
  params:
    rscript=os.environ['projectRoot']+config['src']+"repliseqToRfdValidate.R",
  log:
    o=os.environ['projectRoot']+config['logsDir']+"val.{val}.{genome}.{trainset}.{model}.w{win}.s{step}.o",
    e=os.environ['projectRoot']+config['logsDir']+"val.{val}.{genome}.{trainset}.{model}.w{win}.s{step}.e"
  shell:
    """
    Rscript --vanilla {params.rscript} {input.model} {output.refout} {output.figures} {input.okseq}={input.el} > {log.o} 2> {log.e}
    """



rule writeTrainedModel_IMR_K562_E14TG2a:
  # Note - training library pairs are currently hard-coded.
  input:
    okseq=expand("{lib}.w1000.s1000.okAG.cov",lib=LIBTRAIN),
    repli=expand("{rep}.w{{win}}.s{{step}}.elAG.cov",rep=REPTRAIN)
  output:
    refout="IMR_K562_E14TG2a.{model}.w{win}.s{step}.Rmodel"
  resources:
    mem_mb=4000,
    resources='"rusage[mem=4000]"',
    time="1:00"
  conda:
    os.environ['projectRoot']+config['envsDir']+"base.yaml"
  params:
    rscript=os.environ['projectRoot']+config['src']+"repliseqToRfdWriteTrainer.R",
    trainOnThis="IMR-90.okseq.hg19.w1000.s1000.okAG.cov=IMR-90.elRepli.hg19.w{win}.s{step}.elAG.cov K562.okseq.hg19.w1000.s1000.okAG.cov=K562.elRepli.hg19.w{win}.s{step}.elAG.cov ES-E14TG2a.okseq.bl6.w1000.s1000.okAG.cov=ES-E14TG2a.elRepli.bl6.w{win}.s{step}.elAG.cov"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"wTrainedModel{model}.w{win}.s{step}.o",
    e=os.environ['projectRoot']+config['logsDir']+"wTrainedModel{model}.w{win}.s{step}.e"
  shell:
    """
    Rscript --vanilla {params.rscript} {output.refout} {params.trainOnThis} > {log.o} 2> {log.e}
    """



rule runGrid:
  input:
    expand("grid.g{grid}.w{{win}}.s{{step}}.Rdat",grid=list(range(1,11)))
  output:
    refout="grid.w{win}.s{step}.complete"
  resources:
    mem_mb=1000,
    resources='"rusage[mem=1000]"',
    time="1:00"
  conda:
    os.environ['projectRoot']+config['envsDir']+"base.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"grid.w{win}.s{step}.complete.o",
    e=os.environ['projectRoot']+config['logsDir']+"grid.w{win}.s{step}.complete.e"
  shell:
    """
    date > {output.refout}
    """



rule gridsearch:
  input:
    "doOk.hg19.w1000.s1000.complete",
    "doRepli.hg19.w{win}.s{step}.complete",
    "doOk.bl6.w1000.s1000.complete",
    "doRepli.bl6.w{win}.s{step}.complete",
  output:
    refout="grid.g{grid}.w{win}.s{step}.Rdat"
  resources:
    mem_mb=12000,
    resources='"rusage[mem=12000]"',
    time="40:00"
  params:
    rscript=os.environ['projectRoot']+config['src']+"repliseqToRfdTrainModel.R",
    dataPairs="IMR-90.okseq.hg19.w1000.s1000.okAG.cov=IMR-90.elRepli.hg19.w{win}.s{step}.elAG.cov K562.okseq.hg19.w1000.s1000.okAG.cov=K562.elRepli.hg19.w{win}.s{step}.elAG.cov ES-E14TG2a.okseq.bl6.w1000.s1000.okAG.cov=ES-E14TG2a.elRepli.bl6.w{win}.s{step}.elAG.cov"
  conda:
    os.environ['projectRoot']+config['envsDir']+"base.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"gridSearch.g{grid}.w{win}.s{step}.complete.o",
    e=os.environ['projectRoot']+config['logsDir']+"gridSearch.g{grid}.rw{win}.s{step}.complete.e"
  shell:
    """
    Rscript --vanilla {params.rscript} {output.refout} {params.dataPairs} > {log.o} 2> {log.e}
    """


rule doOK:
  input:
    getSamplesOkCov
  output:
    refout="doOk.{genome}.w{win}.s{step}.complete"
  resources:
    mem_mb=1000,
    resources='"rusage[mem=1000]"',
    time="1:00"
  conda:
    os.environ['projectRoot']+config['envsDir']+"base.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"doOK.{genome}.w{win}.s{step}.complete.o",
    e=os.environ['projectRoot']+config['logsDir']+"doOK.{genome}.w{win}.s{step}.complete.e"
  shell:
    """
    date > {output.refout} 2> {log.e}
    """


rule doRepli:
  input:
    getSamplesRepliCov
  output:
    refout="doRepli.{genome}.w{win}.s{step}.complete"
  resources:
    mem_mb=1000,
    resources='"rusage[mem=1000]"',
    time="1:00"
  conda:
    os.environ['projectRoot']+config['envsDir']+"base.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"doRepli.{genome}.w{win}.s{step}.complete.o",
    e=os.environ['projectRoot']+config['logsDir']+"doRepli.{genome}.w{win}.s{step}.complete.e"
  shell:
    """
    date > {output.refout} 2> {log.e}
    """

 

rule aggregateOkSeq:
  input:
    getReplicatesOk
  output:
    refout="{type}.okseq.{genome}.w{win}.s{step}.okAG.cov"
  resources:
    mem_mb=8000,
    resources='"rusage[mem=8000]"',
    time="1:00"
  params:
    rscript=os.environ['projectRoot']+config['src']+"covAggregate.R"
  conda:
    os.environ['projectRoot']+config['envsDir']+"base.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"agOkSeq.{type}.okseq.{genome}.w{win}.s{step}.complete.o",
    e=os.environ['projectRoot']+config['logsDir']+"agOkSeq.{type}.okseq.{genome}.w{win}.s{step}.complete.e"
  shell:
    """
    Rscript --vanilla {params.rscript} {output.refout} {input} > {log.o} 2> {log.e}
    """

rule aggregateRepliSeq:
  input:
    getReplicatesRepli
  output:
    refout="{type}.elRepli.{genome}.w{win}.s{step}.elAG.cov"
  resources:
    mem_mb=8000,
    resources='"rusage[mem=8000]"',
    time="1:00"
  params:
    rscript=os.environ['projectRoot']+config['src']+"covAggregate.R"
  conda:
    os.environ['projectRoot']+config['envsDir']+"base.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"agRepliSeq.{type}.elRepli.{genome}.w{win}.s{step}.complete.o",
    e=os.environ['projectRoot']+config['logsDir']+"agRepliSeq.{type}.elRepli.{genome}.w{win}.s{step}.complete.e"
  shell:
    """
    Rscript --vanilla {params.rscript} {output.refout} {input} > {log.o} 2> {log.e}
    """


rule elCoverage:
  input:
    bamEarly="{type}.b{brep}.t{trep}.frac1_2.{genome}.sorted.rmdup.bam",
    bamLate="{type}.b{brep}.t{trep}.frac2_2.{genome}.sorted.rmdup.bam",
    bed="{genome}.w{win}.s{step}.bed"
  output:
    refout="{type}.elRepli.b{brep}.t{trep}.{genome}.w{win}.s{step}.cov"
  resources:
    mem_mb=12000,
    resources='"rusage[mem=12000]"',
    time="4:00"
  conda:
    os.environ['projectRoot']+config['envsDir']+"samtools.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"ELcov.{type}.b{brep}.t{trep}.{genome}.w{win}.s{step}.o",
    e=os.environ['projectRoot']+config['logsDir']+"ELcov.{type}.b{brep}.t{trep}.{genome}.w{win}.s{step}.e"
  shell:
    """
    bedtools multicov -bed {input.bed} -bams {input.bamEarly} {input.bamLate} > {output.refout} 2> {log.e}
    """

rule defineGenomeWindows:
  input:
    chromSize=os.environ['projectRoot']+config['genomeDir']+"{genome}.chromSize.main"
  output:
    refout="{genome}.w{win,\d+}.s{step,\d+}.bed"
  resources:
    mem_mb=2000,
    resources='"rusage[mem=2000]"',
    time="1:00"
  params:
    window="{win}",
    step="{step}"
  conda:
    os.environ['projectRoot']+config['envsDir']+"samtools.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"defWindows.{genome}.w{win}.s{step}.o",
    e=os.environ['projectRoot']+config['logsDir']+"defWindows.{genome}.w{win}.s{step}.e"
  shell:
    """
    bedtools makewindows -w {params.window} -s {params.step} -g {input.chromSize} > {output.refout} 2> {log.e}
    """


rule dedupPairedReads:
  input:
    sortedbam = "{type}.elRepli.b{brep}.t{trep}.pe.frac{frac}.{genome}.sorted.bam"
  output:
    refout="{type}.elRepli.b{brep,\d+}.t{trep,\d+}.frac{frac}.{genome}.sorted.rmdup.bam",
    index="{type}.elRepli.b{brep}.t{trep}.frac{frac}.{genome}.sorted.rmdup.bam.bai"
  resources:
    mem_mb=20000,
    resources='"rusage[mem=20000]"',
    time="12:00"
  conda:
    os.environ['projectRoot']+config['envsDir']+"bowtie2.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"dedupPE.{type}.b{brep}.t{trep}.frac{frac}.{genome}.o",
    e=os.environ['projectRoot']+config['logsDir']+"dedupPE.{type}.b{brep}.t{trep}.frac{frac}.{genome}.e"
  shell:
    """
    set +o pipefail;
    samtools sort -n -O BAM {input.sortedbam} | samtools fixmate -m - - | samtools sort | samtools markdup -r -s - {output.refout} 2> {log.e};
    samtools index {output.refout} 2> {log.o}
    """

rule diagnostics:
  input:
    fastq = "{type}.elRepli.b1.t1.p0.frac1_2.fastq.gz"
  output:
    refout = "diagnostics.{type}.out"
  resources:
    mem_mb=1000,
    resources='"rusage[mem=1000]"',
    time="1:00"
  threads: 6
  conda:
    os.environ['projectRoot']+config['envsDir']+"base.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"diagnostics.{type}.o",
    e=os.environ['projectRoot']+config['logsDir']+"diagnostics.{type}.e"
  shell:
    """
    env > {output.refout} 2> {log.e} ;
    which conda >> {output.refout} 2> {log.e};
    date >> {output.refout} 2> {log.e}
    """

rule alignReadsPairedBowtie:
  input:
    fastq1 = "{type}.elRepli.b{brep}.t{trep}.p1.frac{frac}.fastq.gz",
    fastq2 = "{type}.elRepli.b{brep}.t{trep}.p2.frac{frac}.fastq.gz",
    fasta = os.environ['projectRoot']+config['genomeDir']+"{genome}.fa",
    btIndex = os.environ['projectRoot']+config['genomeDir']+"{genome}.1.bt2"
  output:
    refout = "{type}.elRepli.b{brep,\d+}.t{trep,\d+}.pe.frac{frac}.{genome}.sorted.bam",
    index="{type}.elRepli.b{brep,\d+}.t{trep,\d+}.pe.frac{frac}.{genome}.sorted.bam.bai"
  params:
    options = "--no-mixed --no-discordant --reorder",
    fragSize = "-X 1000",
    genomePath = os.environ['projectRoot']+config['genomeDir']+"{genome}"
  resources:
    mem_mb=8000,
    resources='"rusage[mem=8000]"',
    time="12:00",
  threads: 6
  conda:
    os.environ['projectRoot']+config['envsDir']+"bowtie2.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"bowtiePE.{type}.b{brep}.t{trep}.frac{frac}.{genome}.o",
    e=os.environ['projectRoot']+config['logsDir']+"bowtiePE.{type}.b{brep}.t{trep}.frac{frac}.{genome}.e"
  shell:
    """
    set +o pipefail;
    bowtie2 -p 6 -x {params.genomePath} {params.options} {params.fragSize} -1 {input.fastq1} -2 {input.fastq2} 2> {log.e} | samtools view -bSq 20 | samtools sort > {output.refout} 2> {log.o};
    """


rule alignReadsUnpairedBowtie:
  input:
    fastq = "{type}.elRepli.b{brep}.t{trep}.p0.frac{frac}.fastq.gz",
    fasta = os.environ['projectRoot']+config['genomeDir']+"{genome}.fa",
    btIndex=os.environ['projectRoot']+config['genomeDir']+"{genome}.1.bt2"
  output:
    refout = "{type}.elRepli.b{brep,\d+}.t{trep,\d+}.frac{frac}.{genome}.sorted.bam",
    rmdup="{type}.b{brep}.t{trep}.frac{frac}.{genome}.sorted.rmdup.bam"
  params:
    options = "--no-mixed --no-discordant --reorder",
    fragSize = "-X 1000",
    genomePath = os.environ['projectRoot']+config['genomeDir']+"{genome}"
  resources:
    mem_mb=5000,
    resources='"rusage[mem=5000]"',
    time="12:00"
  threads: 6
  conda:
    os.environ['projectRoot']+config['envsDir']+"bowtie2.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"bowtieSE.{type}.b{brep}.t{trep}.frac{frac}.{genome}.o",
    e=os.environ['projectRoot']+config['logsDir']+"bowtieSE.{type}.b{brep}.t{trep}.frac{frac}.{genome}.e"
  shell:
    """
    set +o pipefail;
    bowtie2 -p 6 -x {params.genomePath} {params.options} {params.fragSize} -U {input.fastq} 2> {log.e} | samtools view -bSq 20 | samtools sort > {output.refout} 2> {log.o} ;
    samtools rmdup -S {output.refout} {output.rmdup} 2>> {log.o} ;
    samtools index {output.rmdup} 2>> {log.o} ;
    """

rule buildBowtieIndex:
  input:
    fasta=os.environ['projectRoot']+config['genomeDir']+"{genome}.fa"
  output:
    index=os.environ['projectRoot']+config['genomeDir']+"{genome}.1.bt2",
    refout="{genome}.bowtieIndex.complete"
  params:
    genomePath = os.environ['projectRoot']+config['genomeDir']+"{genome}"
  resources:
    mem_mb=20000,
    resources='"rusage[mem=20000]"',
    time="12:00"
  conda:
    os.environ['projectRoot']+config['envsDir']+"bowtie2.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"bowtieIndex.{genome}.o",
    e=os.environ['projectRoot']+config['logsDir']+"bowtieIndex.{genome}.e"
  shell:
    """
    bowtie2-build {input.fasta} {params.genomePath}
    date > {output.refout}
    """

rule getGenomeHg19:
  output:
    fa=os.environ['projectRoot']+config['genomeDir']+"hg19.fa",
    refout="hg19.download.complete"
  resources:
    mem_mb=2000,
    resources='"rusage[mem=2000]"',
    time="1:00"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"getGenome.hg19.o",
    e=os.environ['projectRoot']+config['logsDir']+"getGenome.hg19.e"
  shell:
    """
    set +o pipefail;
    wget -nd https://hgdownload.soe.ucsc.edu/goldenPath/hg19/bigZips/hg19.fa.gz 2> {log.e}
    gunzip < hg19.fa.gz | perl -walne 's/>chr/>/;print' > {output.fa} 2>> {log.e}
    date > {output.refout}
    """

rule makeChromSizeMain:
  input:
    fa=os.environ['projectRoot']+config['genomeDir']+"{genome}.fa",
  output:
    refout="{genome}.csm.complete",
    csm=os.environ['projectRoot']+config['genomeDir']+"{genome}.chromSize.main"
  resources:
    mem_mb=2000,
    resources='"rusage[mem=2000]"',
    time="1:00"
  conda:
    os.environ['projectRoot']+config['envsDir']+"samtools.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"mkChromSize.{genome}.o",
    e=os.environ['projectRoot']+config['logsDir']+"mkChromSize.{genome}.e"
  shell:
    """
    set +o pipefail;
    samtools faidx {input} 2> {log.e}
    cat {input.fa}.fai | perl -walne '$F[0]=~s/^chr//;if($F[0]=~/^\d+$/){{print"$F[0]\t$F[1]"}}if($F[0]=~/^[XY]$/){{print"$F[0]\t$F[1]"}}' | sort -k1V > {output.csm} 2>> {log.e}
    date > {output.refout}
    """


include: os.environ['projectRoot']+config['rulesDir']+"okSeqProcessing.smk"
