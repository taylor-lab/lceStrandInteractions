import os
import sys
from pathlib import Path
import pandas as pd

# based on Snakefile.kucab.smk

envvars:
  "projectRoot"

configfile:
    os.environ['projectRoot']+"config/config.yaml"

sasFile = os.environ['projectRoot']+"data/mu/lceSAS.tab"

sas = pd.read_csv(sasFile)
sasSelect = sas[~sas['cleanDiagnosis'].isin(['normal']) & sas['cause'].isin(['DEN']) & sas['strain'].isin(['c3h'])]

NODS = list(sasSelect['nodId'])
NODSALL = list(sas['nodId'])
NODSTRAIN = dict(zip(sasSelect['nodId'],sasSelect['strain']))

rule spatialClustersAnalysis:
  input:
    dummy="dummy"
  output:
    refout="{genome}.{rfd}.rfdStrat{stratNum}.tlsClusterCorWithRfd.pdf"
  resources:
    mem_mb=22000,
    resources='"rusage[mem=22000]"',
    time="1:00"
   params:
    rscript=os.environ['projectRoot']+config['src']+"dummy.R",
  conda:
    os.environ['projectRoot']+config['envsDir']+"base.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"tlsCol.{genome}.{rfd}.s{stratNum}.o",
    e=os.environ['projectRoot']+config['logsDir']+"tlsCol.{genome}.{rfd}.s{stratNum}.e"
  shell:
    """
    date > {output.refout}
    """
 

rule generateReplicaitonFiguresRfd:
  input:
    go="{genome}.{rfd}.rfdStrat{stratNum}.rates.genesOn.BOOTS.complete",
    ga="{genome}.{rfd}.rfdStrat{stratNum}.rates.genic.BOOTS.complete",
    ng="{genome}.{rfd}.rfdStrat{stratNum}.rates.nongenic.BOOTS.complete",
    all="{genome}.{rfd}.rfdStrat{stratNum}.rates.all.BOOTS.complete",
    gop="{genome}.genesOnPlusMergeIc.bed",
    gom="{genome}.genesOnMinusMergeIc.bed",
    genic="{genome}.genic.bed",
    rfd="{genome}.{rfd}.all.rfdStrat21.bed",
    elCov="{genome}.{rfd}.elRepli.w1000.s1000.elAG.cov"
  output:
    refout="{genome}.{rfd}.rfdStrat{stratNum}.all.foldTRUE.pdf",
    unfolded="{genome}.{rfd}.rfdStrat{stratNum}.all.foldFALSE.pdf"
  resources:
    mem_mb=6000,
    resources='"rusage[mem=6000]"',
    time="1:00"
  params:
    rscript=os.environ['projectRoot']+config['src']+"treFigures.R",
    goB="{genome}.{rfd}.rfdStrat{stratNum}.rates.genesOn.boot0.Rtab",
    gaB="{genome}.{rfd}.rfdStrat{stratNum}.rates.genic.boot0.Rtab",
    ngB="{genome}.{rfd}.rfdStrat{stratNum}.rates.nongenic.boot0.Rtab",
    allB="{genome}.{rfd}.rfdStrat{stratNum}.rates.all.boot0.Rtab"
  conda:
    os.environ['projectRoot']+config['envsDir']+"base.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"trefigs.{genome}.{rfd}.s{stratNum}.o",
    e=os.environ['projectRoot']+config['logsDir']+"trefigs.{genome}.{rfd}.s{stratNum}.e"
  shell:
    """
    Rscript --vanilla {params.rscript} {output.refout} {input.rfd} {input.gop} {input.gom} {input.genic} {input.elCov} >> {log.o} 2>> {log.e} ;
    Rscript --vanilla {params.rscript} {output.unfolded} {input.rfd} {input.gop} {input.gom} {input.genic} {input.elCov} >> {log.o} 2>> {log.e} ;
    """

rule dirtyHackNameChangeTracking:
  input:
    replicov16="../repliseq/1-6.elRepli.c3h.w1000.s1000.elAG.cov",
    replicov74="../repliseq/Hep74-3a.elRepli.c3h.w1000.s1000.elAG.cov"
  output:
    refout="c3h.mouseLiver3AOkFilt.elRepli.w1000.s1000.elAG.cov",
    cov16="c3h.mouseLiver1OkFilt.elRepli.w1000.s1000.elAG.cov"
  resources:
    mem_mb=1000,
    resources='"rusage[mem=1000]"',
    time="1:00"
  conda:
    os.environ['projectRoot']+config['envsDir']+"base.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"repliseqToReplicationNameTransform.o",
    e=os.environ['projectRoot']+config['logsDir']+"repliseqToReplicationNameTransform.o"
  shell:
    """
    ln -s {input.replicov16} {output.cov16} 2> {log.e} ;
    ln -s {input.replicov74} {output.refout} 2>> {log.e} 
    """

rule generateReplicaitonFiguresEL:
  input:
    go="{genome}.{rfd}.elStrat{stratNum}.rates.genesOn.BOOTS.complete",
    ga="{genome}.{rfd}.elStrat{stratNum}.rates.genic.BOOTS.complete",
    ng="{genome}.{rfd}.elStrat{stratNum}.rates.nongenic.BOOTS.complete",
    all="{genome}.{rfd}.elStrat{stratNum}.rates.all.BOOTS.complete"
  output:
    refout="{genome}.{rfd}.elStrat{stratNum}.all.foldFALSE.pdf"
  resources:
    mem_mb=6000,
    resources='"rusage[mem=6000]"',
    time="1:00"
  params:
    rscript=os.environ['projectRoot']+config['src']+"treFigures.R",
    goB="{genome}.{rfd}.elStrat{stratNum}.rates.genesOn.boot0.Rtab",
    gaB="{genome}.{rfd}.elStrat{stratNum}.rates.genic.boot0.Rtab",
    ngB="{genome}.{rfd}.elStrat{stratNum}.rates.nongenic.boot0.Rtab",
    allB="{genome}.{rfd}.elStrat{stratNum}.rates.all.boot0.Rtab",
  conda:
    os.environ['projectRoot']+config['envsDir']+"base.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"trefigs.{genome}.{rfd}.els{stratNum}.o",
    e=os.environ['projectRoot']+config['logsDir']+"trefigs.{genome}.{rfd}.els{stratNum}.e"
  shell:
    """
    Rscript --vanilla {params.rscript} {output.refout} {params.allB} {params.goB} {params.ngB} >> {log.o} 2>> {log.e} ;
    Rscript --vanilla {params.rscript} {output.unfolded} {params.allB} {params.goB} {params.ngB} >> {log.o} 2>> {log.e} ;
    """


rule runTricompRatesEngineBoots:
  input:
    rtab=expand("{{genome}}.{{rfd}}.{{st}}Strat{{stratNum}}.rates.{{annoCat}}.boot{boot}.Rtab",boot=list(range(0,101)))
  output:
    refout="{genome}.{rfd}.{st}Strat{stratNum}.rates.{annoCat}.BOOTS.complete"
  resources:
    mem_mb=2000,
    resources='"rusage[mem=2000]"',
    time="1:00"
  conda:
    os.environ['projectRoot']+config['envsDir']+"base.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"treBoot.{genome}.{rfd}.{st}s{stratNum}.{annoCat}.o",
    e=os.environ['projectRoot']+config['logsDir']+"treBoot.{genome}.{rfd}.{st}s{stratNum}.{annoCat}.e"
  shell:
    """
    date +%Y%m%d%H%M%S > {output.refout} 2> {log.e}
    """

rule ratesForRegressionAnalysisBed:
  input:
    rtab="../repliseq/{lib}.{genome}.w50000.s10000.elRepToRfd.normTUBBS.Rtab"
  output:
    refout="{lib}.{genome}.elRepToRfd.slices.bed"
  resources:
    mem_mb=12000,
    resources='"rusage[mem=12000]"',
    time="2:00"
  params:
    rscript=os.environ['projectRoot']+config['src']+"rtabToBed.R"
  conda:
    os.environ['projectRoot']+config['envsDir']+"base.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"regTricomp.{genome}.{lib}.o",
    e=os.environ['projectRoot']+config['logsDir']+"regTricompBed.{genome}.{lib}.e"
  shell:
    """
    Rscript --vanilla {params.rscript} {output.refout} {input.rtab} > {log.o} 2> {log.e} ;
    """

rule ratesForRegressionAnalysisTrinuc:
  input:
    bed="{lib}.{genome}.elRepToRfd.slices.bed",
    fa=os.environ['projectRoot']+config['genomeDir']+"{genome}.fa"
  output:
    refout="{lib}.{genome}.elRepToRfd.slices.triComp"
  resources:
    mem_mb=8000,
    resources='"rusage[mem=8000]"',
    time="6:00"
  params:
    pl=os.environ['projectRoot']+config['src']+"streamTriNucCounterNoMerge.pl"
  conda:
    os.environ['projectRoot']+config['envsDir']+"samtools.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"regTricomp.{genome}.{lib}.o",
    e=os.environ['projectRoot']+config['logsDir']+"regTricomp.{genome}.{lib}.e"
  shell:
    """
    bedtools getfasta -tab -name+ -fi {input.fa} -bed {input.bed} | perl {params.pl} > {output.refout} 2>> {log.e} ;
    """

rule runTricompRatesEngine:
  input:
    nods="{genome}.{rfd}.anno.{st}Strat{stratNum}.ALLNODScomplete"
  output:
    refout="{genome}.{rfd}.{st}Strat{stratNum}.rates.{annoCat}.boot{boot}.Rtab"
  resources:
    mem_mb=8000,
    resources='"rusage[mem=8000]"',
    time="6:00"
  params:
    rscript=os.environ['projectRoot']+config['src']+"tricompRatesEngine.R"
  conda:
    os.environ['projectRoot']+config['envsDir']+"base.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"tre.{genome}.{rfd}.{st}s{stratNum}.{annoCat}.{boot}.o",
    e=os.environ['projectRoot']+config['logsDir']+"tre.{genome}.{rfd}.{st}s{stratNum}.{annoCat}.{boot}.e"
  shell:
    """
    Rscript --vanilla {params.rscript} {output.refout} > {log.o} 2> {log.e}
    """

rule runTricompRatesEngineSpectral:
  input:
    nods="{genome}.{rfd}.anno.{st}Strat{stratNum}.ALLNODScomplete"
  output:
    refout="{genome}.{rfd}.{st}Strat{stratNum}.rates.{annoCat}.spectral.boot{boot}.Rtab"
  resources:
    mem_mb=8000,
    resources='"rusage[mem=8000]"',
    time="6:00"
  params:
    rscript=os.environ['projectRoot']+config['src']+"tricompRatesEngineSpectral.R"
  conda:
    os.environ['projectRoot']+config['envsDir']+"base.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"tre.{genome}.{rfd}.{st}s{stratNum}.{annoCat}.{boot}.o",
    e=os.environ['projectRoot']+config['logsDir']+"tre.{genome}.{rfd}.{st}s{stratNum}.{annoCat}.{boot}.e"
  shell:
    """
    Rscript --vanilla {params.rscript} {output.refout} > {log.o} 2> {log.e}
    """

rule doEachNod:
  input:
    expand("{library}.{{genome}}.{{rfd}}.anno.{{st}}Strat{{stratNum}}.complete",library=NODS)
  output:
    refout="{genome}.{rfd}.anno.{st}Strat{stratNum}.ALLNODScomplete"
  resources:
    mem_mb=8000,
    resources='"rusage[mem=8000]"',
    time="2:00"
  conda:
    os.environ['projectRoot']+config['envsDir']+"base.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"doEachNod.{genome}.{rfd}.{st}Strat{stratNum}.o",
    e=os.environ['projectRoot']+config['logsDir']+"doEachNod.{genome}.{rfd}.{st}Strat{stratNum}.e"
  shell:
    """
    date > {output.refout}
    """


rule doPerCloneStratRates:
  input:
    tricompAll="{library}.{genome}.{rfd}.all.{st}Strat{stratNum}.triComp",
    tricompGenic="{library}.{genome}.{rfd}.genic.{st}Strat{stratNum}.triComp",
    tricompNongenic="{library}.{genome}.{rfd}.nongenic.{st}Strat{stratNum}.triComp",
    tricompGenesOn="{library}.{genome}.{rfd}.genesOn.{st}Strat{stratNum}.triComp",
    mutations=os.environ['projectRoot']+config['nodulesDir']+"{library}.nodMat"
  output:
    refout="{library}.{genome}.{rfd}.anno.{st}Strat{stratNum}.complete",
  resources:
    mem_mb=8000,
    resources='"rusage[mem=8000]"',
    time="2:00"
  conda:
    os.environ['projectRoot']+config['envsDir']+"base.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"doPerCloneStratRates.{library}.{genome}.{rfd}.{st}Strat{stratNum}.o",
    e=os.environ['projectRoot']+config['logsDir']+"doPerCloneStratRates.{library}.{genome}.{rfd}.{st}Strat{stratNum}.e"
  shell:
    """
    date > {output.refout}
    """


rule multiStratIntersect:
  input:
    genic="{genome}.{rfd}.genic.{st}Strat{stratNum}.bed",
    nongenic="{genome}.{rfd}.nongenic.{st}Strat{stratNum}.bed",
    all="{genome}.{rfd}.all.{st}Strat{stratNum}.bed",
    kucabSegs="{library}.autoDrcr.all.bed",
    fa=os.environ['projectRoot']+config['genomeDir']+"{genome}.fa",
    genesOn="{genome}.{rfd}.genesOn.{st}Strat{stratNum}.bed"
  output:
    genicBed="{library}.{genome}.{rfd}.genic.{st}Strat{stratNum}.bed",
    genesOnBed="{library}.{genome}.{rfd}.genesOn.{st}Strat{stratNum}.bed",
    nongenicBed="{library}.{genome}.{rfd}.nongenic.{st}Strat{stratNum}.bed",
    allBed="{library}.{genome}.{rfd}.all.{st}Strat{stratNum}.bed",
    refout="{library}.{genome}.{rfd}.genic.{st}Strat{stratNum}.triComp",
    genesOn="{library}.{genome}.{rfd}.genesOn.{st}Strat{stratNum}.triComp",
    nongenic="{library}.{genome}.{rfd}.nongenic.{st}Strat{stratNum}.triComp",
    all="{library}.{genome}.{rfd}.all.{st}Strat{stratNum}.triComp"
  resources:
    mem_mb=8000,
    resources='"rusage[mem=8000]"',
    time="8:00"
  params:
    pl=os.environ['projectRoot']+config['src']+"streamTriNucCounterNoMerge.pl"
  conda:
    os.environ['projectRoot']+config['envsDir']+"samtools.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"rfdmultiStrat.{library}.{genome}.{rfd}.{st}.{stratNum}.o",
    e=os.environ['projectRoot']+config['logsDir']+"rfdmultiStrat.{library}.{genome}.{rfd}.{st}.{stratNum}.e"
  shell:
    """
    bedtools intersect -a {input.genic} -b {input.kucabSegs} > {output.genicBed} 2> {log.e} ;
    bedtools intersect -a {input.genesOn} -b {input.kucabSegs} > {output.genesOnBed} 2>> {log.e} ; 
    bedtools intersect -a {input.nongenic} -b {input.kucabSegs} > {output.nongenicBed} 2>> {log.e} ;
    bedtools intersect -a {input.all} -b {input.kucabSegs} > {output.allBed} 2>> {log.e} ;
    bedtools getfasta -tab -name+ -fi {input.fa} -bed {output.genicBed} | perl {params.pl} > {output.refout} 2>> {log.e} ;
    bedtools getfasta -tab -name+ -fi {input.fa} -bed {output.genesOnBed} | perl {params.pl} > {output.genesOn} 2>> {log.e} ;
    bedtools getfasta -tab -name+ -fi {input.fa} -bed {output.nongenicBed} | perl {params.pl} > {output.nongenic} 2>> {log.e} ;
    bedtools getfasta -tab -name+ -fi {input.fa} -bed {output.allBed} | perl {params.pl} > {output.all} 2>> {log.e} ;
    
    """

rule drcrToBed:
  input:
    kucabSegs=os.environ['projectRoot']+config['nodulesDir']+"{library}.fnodDrcrMa"
  output:
    refout="{library}.autoDrcr.all.bed",
    w="{library}.autoDrcr.watson.bed",
    c="{library}.autoDrcr.crick.bed"
  resources:
    mem_mb=1000,
    resources='"rusage[mem=2000]"',
    time="2:00"
  params:
    rscript=os.environ['projectRoot']+config['src']+"drcrMaTobed.R"
  conda:
    os.environ['projectRoot']+config['envsDir']+"base.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"drcrToBed.{library}.o",
    e=os.environ['projectRoot']+config['logsDir']+"drcrToBed.{library}.e"
  shell:
    """
    Rscript --vanilla {params.rscript} {input.kucabSegs} {output.refout} > {log.o} 2> {log.e} ;
    """

rule rfdToGenicNongenic:
  input:
    rfdStates=os.environ['projectRoot']+config['analysisDir']+"repliseq/{genome}.{rfd}.{st}Strat{stratNum}.bed",
    genic="{genome}.genic.bed",
    nongenic="{genome}.nongenic.bed",
    blacklist=os.environ['projectRoot']+config['genomeDir']+"{genome}.arc.blacklist.bed",
    genesOn="{genome}.genesOnMergeIc.bed",
    genesOnPlus="{genome}.genesOnPlusMergeIc.bed",
    genesOnMinus="{genome}.genesOnMinusMergeIc.bed",
  output:
    refout="{genome}.{rfd}.genic.{st}Strat{stratNum}.bed",
    nongenic="{genome}.{rfd}.nongenic.{st}Strat{stratNum}.bed",
    all="{genome}.{rfd}.all.{st}Strat{stratNum}.bed",
    genesOn="{genome}.{rfd}.genesOn.{st}Strat{stratNum}.bed",
    genesOnPlus="{genome}.{rfd}.genesOnPlus.{st}Strat{stratNum}.bed",
    genesOnMinus="{genome}.{rfd}.genesOnMinus.{st}Strat{stratNum}.bed",
  resources:
    mem_mb=4000,
    resources='"rusage[mem=4000]"',
    time="2:00"
  conda:
    os.environ['projectRoot']+config['envsDir']+"samtools.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"{st}multiStrat.{genome}.{rfd}.{stratNum}.o",
    e=os.environ['projectRoot']+config['logsDir']+"{st}multiStrat.{genome}.{rfd}.{stratNum}.e"
  shell:
    """
    bedtools intersect -a {input.rfdStates} -b {input.genic} > {output.refout} 2> {log.e};
    bedtools intersect -a {input.rfdStates} -b {input.nongenic} > {output.nongenic} 2>> {log.e};
    bedtools subtract -a {input.rfdStates} -b {input.blacklist} > {output.all} 2>> {log.e};
    bedtools intersect -a {input.rfdStates} -b {input.genesOn} > {output.genesOn} 2>> {log.e};
    bedtools intersect -a {input.rfdStates} -b {input.genesOnPlus} > {output.genesOnPlus} 2>> {log.e};
    bedtools intersect -a {input.rfdStates} -b {input.genesOnMinus} > {output.genesOnMinus} 2>> {log.e};
    """



rule gtfMergedTrascriptRegions:
  input:
    gtf=os.environ['projectRoot']+config['genomeDir']+"{genome}.gtf",
    chr=os.environ['projectRoot']+config['genomeDir']+"{genome}.chromSize.bed",
    chrMain=os.environ['projectRoot']+config['genomeDir']+"{genome}.chromSize.main",
    blacklist=os.environ['projectRoot']+config['genomeDir']+"{genome}.arc.blacklist.bed",
    genesOn="{genome}.genesOn.bed",
    genesOnMinus="{genome}.genesOn.minusStrandGene.bed",
    genesOnPlus="{genome}.genesOn.plusStrandGene.bed"
  output:
    refout="{genome}.genic.bed",
    nonGenic="{genome}.nongenic.bed",
    minusBlacklist="{genome}.minusBlacklist.bed",
    genes="{genome}.genicRegions.bed",
    genic="{genome}.genicMerge.bed",
    genicSlop="{genome}.genicSlop.bed",
    genesOnMerge="{genome}.genesOnMerge.bed",
    genesOnMinusMerge="{genome}.genesOnMinusMerge.bed",
    genesOnPlusMerge="{genome}.genesOnPlusMerge.bed",
    genesOnMergeIc="{genome}.genesOnMergeIc.bed",
    genesOnMinusMergeIc="{genome}.genesOnMinusMergeIc.bed",
    genesOnPlusMergeIc="{genome}.genesOnPlusMergeIc.bed",
  resources:
    mem_mb=4000,
    resources='"rusage[mem=4000]"',
    time="2:00"
  params:
    perlScript=os.environ['projectRoot']+config['src']+"gtfToGenic.pl"
  conda:
    os.environ['projectRoot']+config['envsDir']+"samtools.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"bedGymnastics.{genome}.o",
    e=os.environ['projectRoot']+config['logsDir']+"bedGymnastics.{genome}.e"
  shell:
    """
    bedtools subtract -a {input.chr} -b {input.blacklist} > {output.minusBlacklist} 2> {log.e} ;
    perl -w {params.perlScript} {input.gtf} > {output.genes} 2>> {log.e} ;
    bedtools merge -i {output.genes} > {output.genic} 2>> {log.e} ; 
    bedtools slop -i {output.genic} -b 5000 -g {input.chrMain} > {output.genicSlop} 2>> {log.e} ;
    bedtools intersect -a {output.minusBlacklist} -b {output.genic} > {output.refout} 2>> {log.e} ;
    bedtools subtract -a {output.minusBlacklist} -b {output.genicSlop} > {output.nonGenic} 2>> {log.e} ;
    bedtools merge -i {input.genesOn} > {output.genesOnMerge} 2>> {log.e} ;
    bedtools merge -i {input.genesOnMinus} > {output.genesOnMinusMerge} 2>> {log.e} ;
    bedtools merge -i {input.genesOnPlus} > {output.genesOnPlusMerge} 2>> {log.e} ;
    bedtools intersect -a {output.minusBlacklist} -b {output.genesOnMerge} > {output.genesOnMergeIc} ;
    bedtools intersect -a {output.minusBlacklist} -b {output.genesOnPlusMerge} > {output.genesOnPlusMergeIc} ;
    bedtools intersect -a {output.minusBlacklist} -b {output.genesOnMinusMerge} > {output.genesOnMinusMergeIc} ;
    """

rule getOnGenes:
  input:
    exp=os.environ['projectRoot']+config['expressionDir']+"{genome}.P15nascentTotalExp.Rtab"
  output:
    refout="{genome}.genesOn.bed",
    genesOnMinus="{genome}.genesOn.minusStrandGene.bed",
    genesOnPlus="{genome}.genesOn.plusStrandGene.bed"
  resources:
    mem_mb=2000,
    resources='"rusage[mem=2000]"',
    time="1:00"
  params:
    rscript=os.environ['projectRoot']+config['src']+"getOnGenes.R"
  conda:
    os.environ['projectRoot']+config['envsDir']+"base.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"onGenes.{genome}.o",
    e=os.environ['projectRoot']+config['logsDir']+"onGenes.{genome}.e"
  shell:
    """
    Rscript --vanilla {params.rscript} {input.exp} {output.refout} > {log.o} 2> {log.e}
    """

rule chromSizeBed:
  input:
    chrMain=os.environ['projectRoot']+config['genomeDir']+"{genome}.chromSize.main"
  output:
    bed=os.environ['projectRoot']+config['genomeDir']+"{genome}.chromSize.bed",
    refout="{genome}.chromSize.bed.complete"
  resources:
    mem_mb=2000,
    resources='"rusage[mem=2000]"',
    time="2:00"
  conda:
    os.environ['projectRoot']+config['envsDir']+"base.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"chrbed.{genome}.o",
    e=os.environ['projectRoot']+config['logsDir']+"chrbed.{genome}.e"
  shell:
    """
    cat {input.chrMain} | perl -walne 'print "$F[0]\t0\t$F[1]"' > {output.bed} 2> {log.e} ;
    date +%Y%m%d%H%M%S > {output.refout}
    """
