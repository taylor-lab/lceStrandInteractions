

rule strandedReadCoverage:
  input:
    frwrd="{type}.okseq.b{brep}.t{trep}.{genome}.strandF.bam",
    rvrs="{type}.okseq.b{brep}.t{trep}.{genome}.strandR.bam",
    genomeWindows="{genome}.w{win}.s{step}.bed"
  output:
    refout="{type}.okseq.b{brep}.t{trep}.{genome}.w{win}.s{step}.cov"
  resources:
    mem_mb=6000,
    resources='"rusage[mem=6000]"',
    time="6:00"
  conda:
    os.environ['projectRoot']+config['envsDir']+"samtools.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"strCov.{type}.b{brep}.t{trep}.w{win}.s{step}.{genome}.o",
    e=os.environ['projectRoot']+config['logsDir']+"strCov.{type}.b{brep}.t{trep}.w{win}.s{step}.{genome}.e"
  shell:
    """
    bedtools multicov -bed {input.genomeWindows} -bams {input.frwrd} {input.rvrs} > {output.refout} 2> {log.e}
    """




rule readAlginmentStrandReverse:
  input:
    bam="{type}.okseq.b{brep}.t{trep}.{genome}.sorted.rmdup.bam"
  output:
    refout="{type}.okseq.b{brep}.t{trep}.{genome}.strandR.bam"
  resources:
    mem_mb=5000,
    resources='"rusage[mem=5000]"',
    time="2:00"
  conda:
    os.environ['projectRoot']+config['envsDir']+"samtools.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"raStrandR.{type}.b{brep}.t{trep}.{genome}.o",
    e=os.environ['projectRoot']+config['logsDir']+"raStrandR.{type}.b{brep}.t{trep}.{genome}.e"
  shell:
    """
    samtools view {input.bam} -q 30 -f 16 -F 4 -F 256 -O BAM -o {output.refout} > {log.o} 2> {log.e}
    samtools index {output.refout} >> {log.o} 2>> {log.e}
    """



rule readAlginmentStrandForward:
  input:
    bam="{type}.okseq.b{brep}.t{trep}.{genome}.sorted.rmdup.bam"
  output:
    refout="{type}.okseq.b{brep}.t{trep}.{genome}.strandF.bam",
  resources:
    mem_mb=5000,
    resources='"rusage[mem=5000]"',
    time="2:00"
  conda:
    os.environ['projectRoot']+config['envsDir']+"samtools.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"raStrandF.{type}.b{brep}.t{trep}.{genome}.o",
    e=os.environ['projectRoot']+config['logsDir']+"raStrandF.{type}.b{brep}.t{trep}.{genome}.e"
  shell:
    """
    samtools view {input.bam} -q 30 -F 16 -F 4 -F 256 -O BAM -o {output.refout} > {log.o} 2> {log.e}
    samtools index {output.refout} >> {log.o} 2>> {log.e}
    """


rule alignReadsUnpairedBowtieOK:
  input:
    fastq = "{type}.okseq.b{brep}.t{trep}.trimmed.fastq.gz",
    fasta = os.environ['projectRoot']+config['genomeDir']+"{genome}.fa",
    btIndex=os.environ['projectRoot']+config['genomeDir']+"{genome}.1.bt2"
  output:
    refout = "{type}.okseq.b{brep,\d+}.t{trep,\d+}.{genome}.sorted.bam",
    rmdup="{type}.okseq.b{brep}.t{trep}.{genome}.sorted.rmdup.bam"
  params:
    options = "--no-mixed --no-discordant --reorder",
    fragSize = "-X 1000",
    genomePath = os.environ['projectRoot']+config['genomeDir']+"{genome}"
  resources:
    mem_mb=5000,
    resources='"rusage[mem=5000]"',
    time="12:00"
  threads: 6
  conda:
    os.environ['projectRoot']+config['envsDir']+"bowtie2.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"bowtieSE.{type}.b{brep}.t{trep}.{genome}.o",
    e=os.environ['projectRoot']+config['logsDir']+"bowtieSE.{type}.b{brep}.t{trep}.{genome}.e"
  shell:
    """
    bowtie2 -p 6 -x {params.genomePath} {params.options} {params.fragSize} -U {input.fastq} 2> {log.e} | samtools view -bSq 20 | samtools sort > {output.refout} 2> {log.o}
    samtools rmdup -S {output.refout} {output.rmdup} 2>> {log.o}
    samtools index {output.rmdup} 2>> {log.o}
    """


rule trimAdaptorsOK:
  input:
    fastq="{type}.okseq.b{brep}.t{trep}.fastq.gz"
  output:
    refout="{type}.okseq.b{brep}.t{trep}.trimmed.fastq.gz"
  resources:
    mem_mb=20000,
    resources='"rusage[mem=20000]"',
    time="12:00"
  conda:
    os.environ['projectRoot']+config['envsDir']+"adaptors.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"adaptorTrim.{type}.okseq.b{brep}.t{trep}.o",
    e=os.environ['projectRoot']+config['logsDir']+"adaptorTrim.{type}.okseq.b{brep}.t{trep}.e"
  shell:
    """
    cutadapt -e 1 --no-indels -a AGATCGGAAGAGCACACGTCTGAACTCCAGTCA -o {output.refout} --trim-n {input.fastq}
    """



