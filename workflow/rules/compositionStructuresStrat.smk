rule compositionStructuresStrat:
  input:
    bed="{strn}.primaryTranscriptsWindows.q{quant}.win{winsize}.bed",
    fa=os.environ['projectRoot']+config['genomeDir']+"{strn}.arcMask.fa"
  output:
    refout="{strn}.primaryTranscriptsWindows.q{quant,\d+}.win{winsize,\d+}.triComp"
  params:
    pl=os.environ['projectRoot']+config['src']+"streamTriNucCounterNoMerge.pl"
  resources:
    mem_mb=20000,
    resources='"rusage[mem=20000]"',
    time="08:00"
  conda:
    os.environ['projectRoot']+config['envsDir']+"samtools.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"compositionStructuresStrat.{strn}.q{quant}.win{winsize}.o",
    e=os.environ['projectRoot']+config['logsDir']+"compositionStructuresStrat.{strn}.q{quant}.win{winsize}.e"
  shell:
    """
    set +o pipefail;
    bedtools getfasta -tab -name+ -fi {input.fa} -bed {input.bed} | perl {params.pl} > {output.refout} 2> {log.e}
    """
