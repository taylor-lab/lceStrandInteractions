import os
import sys
from pathlib import Path
import pandas as pd

# based on Snakefile.kucab.smk

envvars:
  "projectRoot"

configfile:
    os.environ['projectRoot']+"config/config.yaml"

sasFile = os.environ['projectRoot']+"data/mu/lceSAS.tab"

sas = pd.read_csv(sasFile)
sasSelect = sas.loc[~sas['cleanDiagnosis'].isin(['normal']) & sas['cause'].isin(['DEN']) & sas['strain'].isin(['c3h']) & sas['clonalClass'].isin(['nonWGD'])  & (sas['cellularity'] > 0.5) & (sas['den1sig'] > 0.8)]

NODS = list(sasSelect['nodId'])


rule all:
  input:
    "c3h.clusteredIndels.pdf"

rule phaseCheckIndelClusters:
  input:
    coords="{nod}.indelClust.perm100.filtered",
    perms="{nod}.indelClust.perm100.permStack.filtered"
  output:
    refout="{nod}.indelClust.perm100.filtered.seq",
    permout="{nod}.indelClust.perm100.permStack.filtered.seq"
  resources:
    mem_mb=12000,
    resources='"rusage[mem=12000]"',
    time="08:00",
    cpu=1
  conda:
    os.environ['projectRoot']+config['envsDir']+"samtools.yaml"
  params:
    perlScript=os.environ['projectRoot']+config['srcDir']+"mpileupIndelClusterPhaser.pl",
    fasta=os.environ['projectRoot']+config['genomeDir']+"c3h.fa" 
  log:
    o=os.environ['projectRoot']+config['logsDir']+"phaseCheck.{nod}.o",
    e=os.environ['projectRoot']+config['logsDir']+"phaseCheck.{nod}.e"
  shell:
    """
    perl {params.perlScript} {input.coords} {params.fasta} {output.refout} > {log.o} 2> {log.e} ;
    perl {params.perlScript} {input.perms} {params.fasta} {output.permout} >> {log.o} 2>> {log.e} ;
    """


rule runPermute:
  input:
    expand("{nod}.indelClust.perm100.filtered.seq",nod=NODS)
  output:
    refout="c3h.perm100.complete"
  resources:
    mem_mb=2000,
    resources='"rusage[mem=2000]"',
    time="02:00",
    cpu=1
  conda:
    os.environ['projectRoot']+config['envsDir']+"base.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"complete.indelPerm100.o",
    e=os.environ['projectRoot']+config['logsDir']+"complete.indelPerm100.e"
  shell:
    """
    date > {output.refout} 2> {log.e}
    """


rule indelPermute:
  output:
    refout="{nod}.indelClust.perm100",
    filtered="{nod}.indelClust.perm100.filtered",
    permout="{nod}.indelClust.perm100.permStack.filtered"
  resources:
    mem_mb=32000,
    resources='"rusage[mem=32000]"',
    time="6:00",
    cpu=1
  params:
    rscript=os.environ['projectRoot']+config['srcDir']+"indelClustering.R"
  conda:
    os.environ['projectRoot']+config['envsDir']+"base.yaml"
  log:
    o=os.environ['projectRoot']+config['logsDir']+"{nod}.perm.clusteredIndels.o",
    e=os.environ['projectRoot']+config['logsDir']+"{nod}.perm.clusteredIndels.e"
  shell:
    """
    Rscript --vanilla {params.rscript} {output.refout} > {log.o} 2> {log.e}
    """