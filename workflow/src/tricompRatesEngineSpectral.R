proot<-Sys.getenv("projectRoot")
source(paste0(proot,"workflow/src/lceRbasics.R"))

####
# Derive run variables from output file name.
cliargs<-commandArgs(trailingOnly=TRUE)
if(length(cliargs)==0){
  # expects: 
  #cliargs<-c("c3h.tubbs1k.rfdStrat11.rates.genic.boot0.Rtab")
  cliargs<-c("c3h.mouseLiver3AOkFilt.rfdStrat21.rates.nongenic.spectral.boot0.Rab")
}
refOutFile<-cliargs[1]
imf<-unlist(str_split(refOutFile,"\\."))
strn<-imf[1]
strain<-strn
sampleType<-imf[2]
strat<-imf[3]
annoCat<-imf[5]
bootn<-imf[7]
nboot<-as.numeric(str_match(refOutFile,"\\.boot([0-9]+)\\.")[2])
nbits<-str_match(refOutFile,"\\.([a-z]+)[sS]trat([0-9]+)\\.")
nst<-nbits[2]
nstrat<-nbits[3]
stratLevels<-as.numeric(nstrat)

####

source(paste0(proot,config$srcDir,"doChromOffsets.R"))
diploids<-chromOffsets[expectedPloidy==2,chromId]
muTemplate<-generateMuClasses()



nodList<-nodListMaster<-sas[strain==strn & cause=="DEN" & cleanDiagnosis!="normal" & clonalClass=="nonWGD" & cellularity>.5 & den1sig>.8,nodId]


trinucTabulate<-function(x){
  # expects simple data table with "trinuc" and "N" fields.
  setkey(x,trinuc)
  buildTemplate<-data.table(class=muTemplate$trinuc)
  setkey(buildTemplate,class)
  r<-x[buildTemplate]
  r[is.na(N),N:=0]
  names(r)<-c("trinuc","muN")
  return(r)
}

muTabulate<-function(x){
  # expects simple data table with "trinuc" and "N" fields.
  setkey(x,muClass)
  buildTemplate<-data.table(class=muTemplate$forward)
  setkey(buildTemplate,class)
  r<-x[buildTemplate]
  r[is.na(N),N:=0]
  names(r)<-c("muClass","muN")
  return(r)
}

tcToContexTable<-function(x,ploidy=chromOffsets){
  # expects a tricomp structured data.table 
  # returns the colsums for each context
  catOffset<-which(names(x)=="AAA")
  for(i in 1:dim(ploidy)[1]){
    x[chr==ploidy$chromId[i],catOffset:(catOffset+64):=x[chr==ploidy$chromId[i],catOffset:(catOffset+64)]*ploidy$expectedPloidy[i]]
  }
  cntx<-data.table(trinuc=names(x[,catOffset:(catOffset+64)]),context=colSums(x[,catOffset:(catOffset+64)]))
  cntx[]
}

tabRates<-function(mu,context){
  setkey(mu,trinuc)
  setkey(context,trinuc)
  rt<-context[mu][trinuc!="NNN" & trinuc!="XXX"]
  rt[,ratePerM:=signif((muN/context)*1e6,5)]
  rt[]
}

tabSliceRates<-function(mu,context,drcrThrLow=0.33,drcrThrHigh=2,rfd=1,bootstrap=F){
  contextSlice<-context[Drcr>drcrThrLow & Drcr<=drcrThrHigh & name==rfd]
  cs<-contextSlice
  if(bootstrap==T){
    csLen<-dim(contextSlice)[1]
    cs<-contextSlice[sample(csLen,csLen,replace=T)]
  }
  setkey(mu,idx)
  setkey(cs,idx)
  tritab<-trinucTabulate(mu[cs][!is.na(trinuc)][,.N,by=trinuc])
  ctxtab<-tcToContexTable(cs,ploidy=chromOffsets)
  rtr<-tabRates(mu=tritab,context=ctxtab)
  rtr
}

tabSliceRatesFullCat<-function(mu,context,drcrThrLow=0.33,drcrThrHigh=2,rfd=1,bootstrap=F){
  contextSlice<-context[Drcr>drcrThrLow & Drcr<=drcrThrHigh & name==rfd]
  cs<-contextSlice
  if(bootstrap==T){
    csLen<-dim(contextSlice)[1]
    cs<-contextSlice[sample(csLen,csLen,replace=T)]
  }
  setkey(mu,idx)
  setkey(cs,idx)
  tritab<-muTabulate(mu[cs][!is.na(muClass)][,.N,by=muClass])
  tritab[,trinuc:=gsub("_.","",muClass)]
  ctxtab<-tcToContexTable(cs,ploidy=chromOffsets)
  rtr<-tabRates(mu=tritab,context=ctxtab)
  rtr
}


getNodTabSliceRates<-function(nodId,stratLev=5,tissue="mouseLiver",anno="nongenic",st="rfd",bootstrapNum=0){
  bootTF<-F
  if(bootstrapNum>0){
    bootTF=T
  }
  tc<-fread(file=paste0(nodId,".c3h.",tissue,".",anno,".",st,"Strat",stratLev,".triComp"),header=T)
  tc[,chr:=as.character(chr)]
  tc[,idx:=.I]
  asym<-fread(file=paste0(nodId,".autoDrcr.all.bed"),header=F)
  names(asym)<-c("chr","bedLeft","chrRight","nodId","Drcr")
  asym[,chrLeft:=bedLeft+1]
  mu<-fread(file=paste0(proot,config$nodulesDir,nodId,".nodMatMa"))
  # intersect tc and asym so we know RFD and mutational asymmetry.
  setkey(asym,chr,chrLeft,chrRight)
  setkey(tc,chr,chrLeft,chrRight)
  tcAsym<-foverlaps(tc,asym,type="within",mult="all")
  subMu<-mu[isIndel==0 & is.na(suspectDepth) & (is.na(famClust) | famClust>1e-4),.(chr,pos,muClass,nodId)]
  subMu[,chrLeft:=pos]
  subMu[,chrRight:=pos]
  setkey(subMu,chr,chrLeft,chrRight)
  tcMu<-foverlaps(subMu,tc,type="within",mult="all")[,.(chr,pos,muClass,idx,nodId)][!is.na(idx)]
  # All mutations are now assigned to tc segment
  tcMu[,trinuc:=gsub("_.","",muClass)]
  levList<-list()
  for(l in 1:stratLev){
    lo<-l
    levList[[lo]]<-tabSliceRatesFullCat(mu=tcMu,context=tcAsym,drcrThrLow=0.33,drcrThrHigh=2,rfd=l,bootstrap=bootTF)
    levList[[lo]][,lesionStrand:="w"]
    levList[[lo]][,nodId:=nodId]
    levList[[lo]][,rfdQ:=l]
    lo<-l+stratLev
    levList[[lo]]<-tabSliceRatesFullCat(mu=tcMu,context=tcAsym,drcrThrLow=-2,drcrThrHigh=-0.33,rfd=l,bootstrap=bootTF)
    levList[[lo]][,lesionStrand:="c"]
    levList[[lo]][,nodId:=nodId]
    levList[[lo]][,rfdQ:=l]
    lo<-l+(stratLev*2)
    levList[[lo]]<-tabSliceRatesFullCat(mu=tcMu,context=tcAsym,drcrThrLow=-0.33,drcrThrHigh=0.33,rfd=l,bootstrap=bootTF)
    levList[[lo]][,lesionStrand:="n"]
    levList[[lo]][,nodId:=nodId]
    levList[[lo]][,rfdQ:=l]
  }
  rtTsr<-rbindlist(levList)
  rtTsr
}

nodTsrList<-list()
for(n in 1:length(nodList)){
  nodTsrList[[n]]<-getNodTabSliceRates(nodId=nodList[n],stratLev=stratLevels,anno=annoCat,tissue=sampleType,st=nst,bootstrapNum=nboot)
  print(paste(n,nodList[n],sep=" "))
}
ratesDiced<-rbindlist(nodTsrList)

ratesCollateList<-list()
for(s in 1:stratLevels){
  ratesCollateList[[s]]<-ratesDiced[lesionStrand=="w" & rfdQ==s,.(sum(context),sum(muN)),by=muClass]
  names(ratesCollateList[[s]])<-c("trinuc","sumCntx","sumMu")
  ratesCollateList[[s]][,lesionStrand:="w"]
  ratesCollateList[[s]][,rfdQ:=s]
  ratesCollateList[[s+stratLevels]]<-ratesDiced[lesionStrand=="c" & rfdQ==s,.(sum(context),sum(muN)),by=muClass]
  names(ratesCollateList[[s+stratLevels]])<-c("trinuc","sumCntx","sumMu")
  ratesCollateList[[s+stratLevels]][,lesionStrand:="c"]
  ratesCollateList[[s+stratLevels]][,rfdQ:=s]
  ratesCollateList[[s+(stratLevels*2)]]<-ratesDiced[lesionStrand=="n" & rfdQ==s,.(sum(context),sum(muN)),by=muClass]
  names(ratesCollateList[[s+(stratLevels*2)]])<-c("trinuc","sumCntx","sumMu")
  ratesCollateList[[s+(stratLevels*2)]][,lesionStrand:="n"]
  ratesCollateList[[s+(stratLevels*2)]][,rfdQ:=s]
}
rateCat<-rbindlist(ratesCollateList)
rateCat[,ratePerM:=(sumMu*1e6)/sumCntx]

if(1==2){
  rateCat[lesionStrand=="w",mean(ratePerM),by=rfdQ]
  rateCat[lesionStrand=="c",mean(ratePerM),by=rfdQ]
  rateCat[lesionStrand=="n",mean(ratePerM),by=rfdQ]
}

orderMu<-function(x,order=muTemplate$forward){
  mo<-data.table(muClass=order)
  mo[,index:=.I]
  setkey(mo,muClass)
  setkey(x,trinuc)
  x[mo][order(index)]
}

pcScaler<-function(x){
  # abs() so scaling also works on subtracted vectors with negative values.
  (x/sum(abs(x)))*100
}

permuteSpectra<-function(x,y,perms=100,tail="lower"){
  rt<-list()
  rt$obs<-1-as.numeric(cosineDist(matrix(c(x,y),2,length(x),byrow=T)))
  rt$perm<-rep(as.numeric(NA),perms)
  xrle<-list()
  xrle$values=c(1:192)
  xrle$lengths=x
  ixrle<-inverse.rle(xrle)
  yrle<-list()
  yrle$values=c(1:192)
  yrle$lengths=y
  iyrle<-inverse.rle(yrle)
  nx<-sum(x)
  ny<-sum(y)
  for(p in 1:perms){
    crucible<-sample(c(ixrle,iyrle),nx+ny,replace=F)
    newx<-data.table(cls=crucible[1:nx])[,.N,by=cls]
    newy<-data.table(cls=crucible[(nx+1):(nx+ny)])[,.N,by=cls]
    vx<-rep(0,192)
    vy<-rep(0,192)
    vx[newx$cls]=newx$N 
    vy[newy$cls]=newy$N 
    rt$perm[p]<-1-as.numeric(cosineDist(matrix(c(vx,vy),2,length(x),byrow=T)))
  }
  if(tail=="lower"){
    rt$p=sum(rt$perm<=rt$obs)/perms
  }
  if(tail=="upper"){
    rt$p=sum(rt$perm>=rt$obs)/perms
  }
  if(rt$p==0){
    rt$p=signif(.99/perms,3)
  }
  return(rt)
}

#plotSpectrumCompact192(pcScaler(muV1))
#plotSpectrumCompact192(pcScaler(muV2[muTemplate$forToRevCompLook]))

pmat<-matrix(0,21,21)
cmat<-matrix(0,21,21)

for(lx in 1:21){
  for(ly in 1:21){
    if(lx>ly){
      muV1<-orderMu(rateCat[lesionStrand=="w" & rfdQ==lx,sum(sumMu),by=trinuc])$V1
      muV2<-orderMu(rateCat[lesionStrand=="w" & rfdQ==ly,sum(sumMu),by=trinuc])$V1
    }
    if(lx<=ly){
      muV1<-orderMu(rateCat[lesionStrand=="w" & rfdQ==lx,sum(sumMu),by=trinuc])$V1
      muV2<-orderMu(rateCat[lesionStrand=="c" & rfdQ==ly,sum(sumMu),by=trinuc])$V1[muTemplate$forToRevCompLook]
    }
    ps<-permuteSpectra(muV1,muV2,perms=1000)
    pmat[lx,ly]=ps$p
    cmat[lx,ly]=ps$obs
    print(paste(lx,ly,sep=" "))
  }
}

save(rateCat,pmat,cmat,file=refOutFile)

