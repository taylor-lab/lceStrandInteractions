use strict;
# given a GTF file, identify genic extent annotation and spit it out as
# a bed format stream with strandedness.

while(<>){
  chomp;
  next if (/^#/);
  my @sp = split /\t/;
  my $name ="NULL";
  # Only working with main assembly autosomes and X
  next if($sp[0] !~ /^[0-9X]+$/);
  if($sp[2] eq "gene"){
    if($sp[8]=~/gene_id "(\w+)"/){
      $name=$1;
    }
    $sp[3] -= 1; # convert to bedish
    print join "\t", ($sp[0],$sp[3],$sp[4],$name,100,$sp[6]) ;
    print "\n";
  }
}

