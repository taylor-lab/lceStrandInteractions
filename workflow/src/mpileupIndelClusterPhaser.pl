# call samtools mpileup to extract a slice of consecutive nucleotide positions
# to quantify co-phasing of proximal mutations then produce ancestral & derived sequence pairs
# for alignment ambiguity scoring.
# Uses the -r byte index approach rather than -l scanning and filtering. latter is better for large numbers from
# the same bam. Here we typically have small numbers distributed sites. Takes about 1 second per call into a
# large WGS bam.
use strict;

my $debug = 1;
my $nullExpect = 0;
my ($listOfSites,$fasta,$output) = @ARGV;
if ($output =~ /\.nullExpect\./){
  $nullExpect = 1;
}
# ($listOfSites,$fasta,$output) = ("demo.Rtab","/exports/igmm/eddie/TCGA_exome/martin/lce-si/data/genomes/c3h.fa","out.tsv");
open(OF,">$output") or die "Failed to write $output";
open(SITES, "<$listOfSites") or die "Failed to read $listOfSites\n";
my $nodId = "NA";
my $bamPath=$ENV{'projectRoot'}."data/alignments_20171018_link";
opendir(my $bamH, $bamPath);
my @bamList =  grep {/\.bam$/} readdir($bamH);

my $flank=100;
my @atoSeqTemplate = split //, "N"x($flank*2+1);
while(<SITES>){
  chomp;
  my @sp = split /,/;
  if($sp[0] eq "chr"){
    # header
    for my $i (0 .. $#sp){
      print "$i\t$sp[$i]\n";
    }
    next;
  }
  # find bams (could be more than one per nodule)
  my $len = length($sp[5])-1;
  my $cLeft=$sp[2]-$flank;
  my $cRight=$sp[3]+$flank;
  my $eventSize = $sp[7];
  if($eventSize < 0){
    $cRight = $cRight + abs($eventSize);
  }
  my $r = "$sp[0]:$cLeft-$cRight";
  my $idPos = $sp[2];
  my ($nfrom,$nto) = ("N","N");
  my @bamTargets = grep(/\Q_$sp[4]/,@bamList);
  my $lesionStrand = $sp[20];
  my $fullBamPath = "$bamPath/$bamTargets[0]";
  # The -B option is important here, otherwise BAQ filtering in mpileup will suppress reporting of
  # clustered in-phase indels with substitutions which is exactly what we are looking for.
  my $construct = "samtools mpileup $fullBamPath -Q 20 -B --ff UNMAP,SECONDARY,QCFAIL,DUP -a --output-QNAME -f $fasta -r $r";
  my $rightSv = $sp[15];
  my $leftSv = $sp[16];
  my $permNo = $sp[17];
  my $sbs = 0 - $leftSv;
  my $subPos = $sp[2] + $sbs;
  if($rightSv < $leftSv){
    $sbs = $rightSv;
    if ($eventSize<0){
      $subPos = $sp[3] + $sbs; 
    } else {
      $subPos = $sp[3] + $sbs;
    }
  }
   if ($debug == 1){
    print "$construct \n";
  }
  open(MP, "$construct |");

  my @atoSeq = @atoSeqTemplate;
  my $relPos = 0;
  my @idName;
  my @sbName;
  ###
  # Calculate expected indel identity..
  my $indelMotif = fromToCoder($sp[5],$sp[6]);
  my $sbsMotif = "N";
  my $sbsTopAlt = "N";
  ###
  #print join ",", ($sp[0],$sp[2],$sp[3],$sp[4],$sp[5],$sp[6],$idPos,$subPos,$leftSv,$rightSv);
  #print " $idPos $subPos\n";
  my $muidpos = "$sp[0]:$sp[1]";
  my ($idIndex,$subIndex) = (0,0);
  my ($idsIndel,$overlapIndel,$idsSbs,$overlapSbs);
  while(<MP>){
    chomp;
    my @mp = split /\t/, $_;
    #print ">$mp[0]\t$mp[1]\t$mp[2]\t$mp[3]\t$mp[4]\n";
    $atoSeq[$relPos]=$mp[2];
    if($mp[1]==$idPos){
      my $stack = $mp[4];
      #print "numbases: $mp[3]\n";
      ($idsIndel,$overlapIndel) = extractSeqId($indelMotif,$stack,$mp[6],$muidpos);
      $idIndex=$relPos;
      #print "indPos: $stack\n";
    }
    if ($mp[1]==$subPos){
      my $stack = $mp[4];
      #print "numbases: $mp[3]\n";
      #print "subPos: $stack\n";
      ($idsSbs,$overlapSbs) = extractSeqId($sbsMotif,$stack,$mp[6],$muidpos);
      $subIndex=$relPos;
      $sbsTopAlt = subConsensus($stack);
      if(!defined $sbsTopAlt){
        $sbsTopAlt = "N";
      }
    }
    my $rt= "NA";
    $relPos++;
  }
  my $ref = join "", @atoSeq;
  my @atoDerived = @atoSeq;
  $atoDerived[$subIndex] = $sbsTopAlt;
  if(length($sp[6])>1){
    # Insertion
    $atoDerived[$idIndex] = lc($sp[6]);
  } 
  my $derived = join "", @atoDerived;
  if (length($sp[5])>1){
    # deletion 
    my $del = substr($derived,$idIndex+1,length($sp[5])-1,"")
  }
  if ($debug==1){
    print "$derived\n";
  }
  my ($hasBothMutations) = intersectIds($idsIndel,$idsSbs);
  my ($spansBothMutations) = intersectIds($overlapIndel,$overlapSbs);
  my ($spansBothHasIndel) = intersectIds($idsIndel,$spansBothMutations);
  my ($spansBothHasSbs) = intersectIds($idsSbs,$spansBothMutations);
  #print "BothMu: $#{$hasBothMutations}\n";
  #print "SpanBoth: $#{$spansBothMutations} $#{$spansBothHasIndel} $#{$spansBothHasSbs}\n\n\n";
  if ($lesionStrand eq "forward"){
    # Reverse complement the sequences - lesions are on the forward strand so minus orientation is
    # new synthesis.  
    $ref = revcomp($ref);
    $derived = revcomp($derived);
  }
  my $hbm = $#{$hasBothMutations}+1;
  my $sbm = $#{$spansBothMutations}+1;
  my $sbhi = $#{$spansBothHasIndel}+1;
  my $sbhs = $#{$spansBothHasSbs}+1;
  print OF join ",", ($ref,$derived,$sp[0],$sp[1],$sp[4],$sp[7],$sp[13],$hbm,$sbm,$sbhi,$sbhs,$lesionStrand,$permNo);
  print OF "\n";
}
close OF;


sub revcomp {
  my ($seq) = @_;
  my $rt = reverse($seq);
  $rt =~ y/ATCGatcg/TAGCtagc/;
  return($rt);
}

sub fromToCoder {
  my ($f,$t) = @_;
  my $ret = "NA";
  if (length($f)>1){
    # deletion
    $f=~s/^.//;
    my $l = length($f);
    $f = uc($f);
    $ret="-$l$f";
  } elsif (length($t)>1){
    $t=~s/^.//;
    my $l = length($t);
    $f = uc($t);
    $ret="+$l$t"
  } else {
    # substitution
    $ret = uc($t);
  }
  return($ret);
}

sub extractSeqId {
  my ($motif,$mpile,$ids,$ident) = @_;
  #print "$mpile\n";
  $mpile = uc($mpile);
  if($motif eq "N"){
    # look for any non-ref base (lets us count multiallelic phasing)
    $mpile =~ s/[ATCG]/N/g;
  }
  if ($motif =~ /^[-\+]/){
    $mpile =~ s/.\Q$motif/y/g;
  } else {
    $mpile =~ s/\Q$motif/y/g;
  }
  $mpile =~ s/\^.//g; # remove read start indicator and associated qual character
  $mpile =~ s/\$//g; # remove read end indicator
  #print "$mpile\n";
  my $recovery = "";
  while ($mpile=~s/([\+\-])(\d+)([><ACGTNacgtn]+)//g){
    my $rem = length($3)-$2;
    if ($rem > 0){
	    $recovery .= substr($3,-$2,$rem);
	   }
  }
  if (length($recovery) > 0){
	   $mpile .= $recovery;
  }
  my @idList = split /,/, $ids;
  my @stack = split //, $mpile;
  my @focal = grep {$stack[$_] =~ /y/} 0 .. $#stack;
  #print "$mpile\n";
  #print "Nstack: $#stack\n";
  #print "Nidlst: $#idList\n";
  #print "$ids\n";

  if ($#stack != $#idList){
    print STDERR "Warning: mpile stack does not match $ident\n";
  }
  #print join ",", @focal;
  #print "\n";
  #print join ",", @idList[@focal];
  #print "\n";
  my @idReturnList = @idList[@focal];
  return(\@idReturnList,\@idList);
}

sub intersectIds {
  my ($refSetA,$refSetB) = @_;
  my %lookup;
  @lookup{@{$refSetA}} = (1) x @{$refSetA};
  my @readIntersection = grep { $lookup{$_} } @{$refSetB};
  return(\@readIntersection);
}

sub subConsensus {
  my ($mpile) = @_;
  $mpile = uc($mpile);
  $mpile =~ s/\^.//g; # remove read start indicator and associated qual character
  $mpile =~ s/\$//g; # remove read end indicator
  my $recovery = "";
  while ($mpile=~s/([\+\-])(\d+)([><ACGTNacgtn]+)//g){
    my $rem = length($3)-$2;
    if ($rem > 0){
	    $recovery .= substr($3,-$2,$rem);
	   }
  }
  if (length($recovery) > 0){
	   $mpile .= $recovery;
  }
  $mpile =~s/[,\.\*]//g;
  my @alts = split //, $mpile;
  my %arank;
  # default to unkown
  $arank{"N"}=1;
  foreach my $n (@alts){
    $arank{$n}++;
  }
  my @ranked = sort {$arank{$b} <=> $arank{$a}} keys %arank;
  return($ranked[0]);
}