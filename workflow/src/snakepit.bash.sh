# Eddie cluster can't submit jobs from your standard node
#qsub -N snakeMaster -l h_rt=72:00:00 -o ../runlogs/snakeMaster.`now`.o -e ../runlogs/snakeMaster.`now`.e -V -b y -cwd 'snakemake -j 400 --cluster-config ../src/cluster.json --cluster "qsub -V -cwd -l h_rt={cluster.time}:00 -l h_vmem={cluster.memory}m -N {cluster.name} -o {cluster.output} -e {cluster.error} -b y"' $@

snakemake --cores 6 $@ 2> ${projectRoot}/logs/snakeMaster.`now`.e 1> ${projectRoot}/logs/snakeMaster.`now`.o &
