Analysis in this directory ${base}/analysis/repliseq requires the download of public files
that cannot be readily automated (require authenticated access and dynamically generated
metadata tsv files).

Files and metadata file are from the 4DN Data Portal https://data.4dnucleome.org/ 

Once fastq files have been obtained, run ${base}/workflow/src/process4dmeta.pl on the metadata
file to generate symbolic links in this directory that can then be used by the snakemake
pipeline.

