# Source this file to generate a project initialisation script and
# build the basic conda environment(s).
export base=`pwd`
export projectName=`basename ${base}`
mamba env create --prefix ${base}/workflow/conda/${projectName}-base -f ${base}/workflow/envs/base.yaml 
mamba env create --prefix ${base}/workflow/conda/${projectName}-samtools -f ${base}/workflow/envs/samtools.yaml 
