# LCE -Strand Interactions
**Strand-resolved mutagenicity of DNA damage and repair**
*Anderson et al, Nature 2024*

# Authors
Code was principally written by Martin Taylor with contributions from Craig Anderson, Lana Talmane, John Connelly and Jan Verburg

# Contact
martin.taylor@ed.ac.uk

# Description
A record of the code and analysis reported in Anderson et al, 2024 to investigate the strand specificity of DNA damage, repair and its subsequent mutagenicity. This code is not intended as a generalisable and portable software package, rather serves to allow for reproduction of the reported analyses. Code within the repository could be adapted for more generalised analysis and has been licensed to allow this.

The analysis and figure generation is broken down into tiers of analysis. The top-tier of analysis takes pre-computed data objects that are provided as part of this repository and produces figure panels and statistics reported in the manuscript. Top-teir analysis will run in a unix commandline environment on lap-top scale hardware, and complete within a few minutes to hours.

Lower-tier analyses produce the data objects required for top-tier analysis. Lower-tier analyses require a cluster computing environment with job submission choreographed by Snakemake. From raw input data to final analysis requires several hundred compute hours, though much of this can be conducted as parallel analysis.

# Installation
## Requirements
* Maximum single-thread memory requirement is 64 GB.
* Per compute node, the maximum assumed CPU core count is 12.
